#! /usr/bin/guile \
-e main
!#

(use-modules (ice-9 format))

;; 定义成符号类型
;(define *table*
;    '(t s r n m k p u h g e f d c b a))
;;  0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
;;(define *table* (list t s r n m k p u h g e f d c b a))

; define a string (MSB)       (LSB)
(define *table* "tsrnmkpuhgefdcba")

(define *segtab*
  '(
;; SPACE ! " # $ % & '
    (" " "")
    ("!" "bmnt")
    ("\"" "mc")
    ("#" "abupmscd")
    ("$" "bahupdefms")
    ("%" "ahmupdsent")
    ("&" "efgumakr")
    ("'" "m")
;; ( ) * +  - . /
    ("(" "nr")
    (")" "kt")
    ("*" "kmnprstu")
    ("+" "upms")
    ("," "t")
    ("-" "up")
    ("." "r")
    ("/" "nt")
;; 0 1 2 3 4 5 6 7
    ("0" "abcdefghnt")
    ("1" "cdn")
    ("2" "abcptfe")
    ("3" "abnpdef")
    ("4" "hupms")
    ("5" "hurefab")
    ("6" "bahgfedpu")
    ("7" "abnt")
;; 8 9 : ; < = > ?
    ("8" "abcdefghup")
    ("9" "abcdefhup")
    (":" "au")
    (";" "aut")
    ("<" "ntfe")
    ("=" "abup")
    (">" "kref")
    ("?" "habcps")
;; @ A B C D E F G
    ("@" "abcdefgus")
    ("A" "ghkru")
    ("B" "abcdefmps")
    ("C" "abefgh")
    ("D" "abcdefms")
    ("E" "abefghu")
    ("F" "abghpu")
    ("G" "abdefghp")
 ;; H I J K L M N O
    ("H" "hgcdpu")
    ("I" "abefms")
    ("J" "cdefg")
    ("K" "ghnru")
    ("L" "efgh")
    ("M" "cdghkn")
    ("N" "cdghkr")
    ("O" "abcdefgh")
;; P Q R S T U V W
    ("P" "abcghpu")
    ("Q" "abcdefghr")
    ("R" "abcghpru")
    ("S" "bakrefg")
    ("T" "abms")
    ("U" "cdghef")
    ("V" "ghnt")
    ("W" "cdghrt")
;; X Y Z [ | ] ^ _
    ("X" "knrt")
    ("Y" "kns")
    ("Z" "abntfe")
    ("[" "bmse")
    ("\\" "kr")
    ("]" "amsf")
    ("^" "hk")
    ("_" "fe")
;; ` a b c d e f g
    ("`" "k")
    ("a" "amseugf")
    ("b" "fghsu")
    ("c" "fgu")
    ("d" "msugfe")
    ("e" "tugfe")
    ("f" "upbms")
    ("g" "bcdemp")
;; h i j k l m n o
    ("h" "mspd")
    ("i" "d")
    ("j" "msf")
    ("k" "msnr")
    ("l" "mse")
    ("m" "guspd")
    ("n" "spd")
    ("o" "spde")
;; p q r s t u v w
    ("p" "msbcp")
    ("q" "mbcdp")
    ("r" "ps")
    ("s" "pre")
    ("t" "mseup")
    ("u" "gfse")
    ("v" "rd")
    ("w" "gfsed")
;; x y z ( | } ~
    ("x" "ntkr")
    ("y" "mpcde")
    ("z" "utf")
    ("{" "bmseu")
    ("|" "ms")
    ("}" "amsfp")
    ("~" "hkm")))

(define (get-v k)
  (let ((v (assoc-ref *segtab* k)))
    (and v (car v))))

;; header
(define *header*
"#ifndef FONT16SEG_H
#define FONT16SEG_H

// standard ascii 16 segment LED font
// defines ascii characters 0x20-0x7E (32-126)
const uint16_t font16seg[] PROGMEM = {
")

;; footer
(define *footer*
"};

#endif
")

(define (gen-num v)
  (define out 0)
  (string-for-each 
   (lambda (c)
     (let ((i (string-index *table* c)))
       (and i (set! out (logior out (ash 1 i))))))
   v)
  out)

(define (main . args)
  (display *header*)
  (for-each
   (lambda (x)
     (let ((k (car x))
	   (v (cadr x)))
       (format #t "0x~4,'0x, // '~a'~%" (gen-num v) k)))
   *segtab*)
  (display *footer*))
	 
