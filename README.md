Intro
=====

This tool is used to generate font table for 16-segment LED display.


Usage
=====

./sixteen > font16seg.h

or

make font

scheme version
==============

The scheme version (in scheme directory) is written by [Nala Ginrut](https://gitlab.com/NalaGinrut)

