/*
 * A tool used to make font table for 16 segment LED.
 *
 * Todo:
 * 1. Output to a file.
 * http://en.wikipedia.org/wiki/C_file_input/output
 *
 * Atommann
 * May 31, 2013
 *
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define NUM_CHARS 95

#define HEADER "#ifndef FONT16SEG_H\n\
#define FONT16SEG_H\n\n\
// standard ascii 16 segment LED font\n\
// defines ascii characters 0x20-0x7E (32-126)\n\
const uint16_t font16seg[] PROGMEM = {"

#define FOOTER "};\n\n\
#endif\n"


//A, B, C, D, E, F, G, H, K, M, N, P, S, R, T, U,
// evil mad science
//G, T, S, U, H, K, M, A, B, N, R, D, E, F, P, C,
// http://harald.studiokubota.com/wordpress/index.php/2012/01/03/more-16-segment-led-fun/
// A1 = a
// A2 = b
// b = c
// c = d
// d1 = f
// d2 = e
// e = g
// f = h
// g1 = u
// g2 = p
// h = k
// i = m
// j = n
// k = r
// l = s
// m = t
//                             F  E D C  B  A 9 8  7  6 5 4 3 2 1 0
// Segment bit order is (MSB) A1 A2 B C D1 D2 E F G1 G2 H I J K L M (LSB)

char segment[] = "tsrnmkpuhgefdcba";

int find_index(char *s, char find)
{
    int index;

    const char *ptr = strchr(s, find);
    if (ptr) index = ptr - s;
}


struct sixteen_seg_tab {
    const char inchar;
    const char *lit;
};

// 95 printable characters
// From 0x20(Space) to 0x7E(~)
static const struct sixteen_seg_tab segtab[] = {
// SPACE ! " # $ % & '
    {' ', ""},
    {'!', "bmnt"},
    {'"', "mc"},
    {'#', "abupmscd"},
    {'$', "bahupdefms"},
    {'%', "ahmupdsent"},
    {'&', "efgumakr"},
    {'\'', "m"},
// ( ) * + , - . /
    {'(', "nr"},
    {')', "kt"},
    {'*', "kmnprstu"},
    {'+', "upms"},
    {',', "t"},
    {'-', "up"},
    {'.', "r"},
    {'/', "nt"},
// 0 1 2 3 4 5 6 7
    {'0', "abcdefghnt"},
    {'1', "cdn"},
    {'2', "abcptfe"},
    {'3', "abnpdef"},
    {'4', "hupms"},
    {'5', "hurefab"},
    {'6', "bahgfedpu"},
    {'7', "abnt"},
// 8 9 : ; < = > ?
    {'8', "abcdefghup"},
    {'9', "abcdefhup"},
    {':', "au"},
    {';', "aut"},
    {'<', "ntfe"},
    {'=', "abup"},
    {'>', "kref"},
    {'?', "habcps"},
// @ A B C D E F G
    {'@', "abcdefgus"},
    {'A', "ghkru"},
    {'B', "abcdefmps"},
    {'C', "abefgh"},
    {'D', "abcdefms"},
    {'E', "abefghu"},
    {'F', "abghpu"},
    {'G', "abdefghp"},
 // H I J K L M N O
    {'H', "hgcdpu"},
    {'I', "abefms"},
    {'J', "cdefg"},
    {'K', "ghnru"},
    {'L', "efgh"},
    {'M', "cdghkn"},
    {'N', "cdghkr"},
    {'O', "abcdefgh"},
// P Q R S T U V W
    {'P', "abcghpu"},
    {'Q', "abcdefghr"},
    {'R', "abcghpru"},
    {'S', "bakrefg"},
    {'T', "abms"},
    {'U', "cdghef"},
    {'V', "ghnt"},
    {'W', "cdghrt"},
// X Y Z [ | ] ^ _
    {'X', "knrt"},
    {'Y', "kns"},
    {'Z', "abntfe"},
    {'[', "bmse"},
    {'\\', "kr"},
    {']', "amsf"},
    {'^', "hk"},
    {'_', "fe"},
// ` a b c d e f g
    {'`', "k"},
    {'a', "amseugf"},
    {'b', "fghsu"},
    {'c', "fgu"},
    {'d', "msugfe"},
    {'e', "tugfe"},
    {'f', "upbms"},
    {'g', "bcdemp"},
// h i j k l m n o
    {'h', "mspd"},
    {'i', "d"},
    {'j', "msf"},
    {'k', "msnr"},
    {'l', "mse"},
    {'m', "guspd"},
    {'n', "spd"},
    {'o', "spde"},
// p q r s t u v w
    {'p', "msbcp"},
    {'q', "mbcdp"},
    {'r', "ps"},
    {'s', "pre"},
    {'t', "mseup"},
    {'u', "gfse"},
    {'v', "rd"},
    {'w', "gfsed"},
// x y z { | } ~
    {'x', "ntkr"},
    {'y', "mpcde"},
    {'z', "utf"},
    {'{', "bmseu"},
    {'|', "ms"},
    {'}', "amsfp"},
    {'~', "hkm"},
    {'\0', ""},
};

int main(void)
{
    int i;
    char litseg;
    uint16_t out = 0;
    const struct sixteen_seg_tab *s;

    printf(HEADER);

    for (s = segtab; s != NULL && s->inchar != '\0'; s++)
    {
        i = 0;
        out = 0;

        while (litseg = s->lit[i++])
            out |= 1 << find_index(segment, litseg);

        if (s->inchar == ' ')
            printf("    0x%04x, // (space)\n", out);
        else
            printf("    0x%04x, // %c\n", out, s->inchar);
    }

    printf(FOOTER);

    return 0;
}

